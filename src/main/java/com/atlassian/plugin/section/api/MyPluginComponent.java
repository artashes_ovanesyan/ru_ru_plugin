package com.atlassian.plugin.section.api;

public interface MyPluginComponent
{
    String getName();
}